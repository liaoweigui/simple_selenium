# -*- coding: utf-8 -*-


import os
import time


class Config(object):
    """
        建议:
            常量变量使用字母大写命名
            如果使用 PyCharm 的开发工具，设置忽视大小写，自动化提示更方便。 设置路径如下：setting->Editor->General->Code Completion 右边取消勾选Match case
    """

    """================测试数据常量（URL，USERNAME,PASSWORD,DB）======================="""
    """测试地址"""
    URL = "http://uatnuwa.yicartrip.com"
    """登录用户名"""
    USERNAME = "xl"
    """登录用户密码"""
    PASSWORD = "Hh888888"
    """数据库地址"""
    DB_URL = ""
    """数据库用户名"""
    DB_USERNAME = ""
    """数据库用户密码"""
    DB_PASSWORD = ""

    "================测试报告描述常量(报告常量请使用REPORT开头)======================="
    """测试报告需要的描述"""
    REPORT_DESCRIPTION = '详细测试结果如下:'
    """测试报告需要的title"""
    REPORT_TITLE = '自动化测试报告'

    "================时间常量(时间常量请使用TIME开头)======================="
    TIME_FORMAT_SIMPLE = time.strftime("%Y-%m-%d_%H_%M_%S")
    TIME_TIME_FORMAT_FULL = time.strftime('%Y{y}%m{m}%d{d}%H{h}%M{f}%S{s}').format(y='年', m='月', d='日', h='时', f='分',
                                                                                   s='秒')

    "================项目路径常量(项目常量请使用PATH开头)======================="
    """项目主目录"""
    PATH_PROJECT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    """当前文件目录"""
    PATH_CURRENT_FILE = os.getcwd()
    """测试用例目录"""
    PATH_TESTCASE = PATH_PROJECT + "\\testcase"
    """测试报告目录"""
    PATH_REPORT = PATH_PROJECT + "\\report" + "\\report_" + TIME_FORMAT_SIMPLE
    """报告截图目录"""
    PATH_REPORT_SCREENSHOT = PATH_PROJECT + "\\report\\tempimg"
    """资源目录"""
    PATH_RESOURCES = PATH_PROJECT + "\\resources"

    "================文件路径常量(项目常量请使用FILE_PATH开头)======================="
    """测试报告文件保存路径"""
    FILE_PATH_REPORT = PATH_REPORT + "\\" + TIME_FORMAT_SIMPLE +".html"
    """异常截图保存路径"""
    FILE_PATH_ERROR_SCREENSHOT = PATH_REPORT_SCREENSHOT + "\\" + TIME_TIME_FORMAT_FULL + ".png"

    "================其他常量======================="


if __name__ == '__main__':
    print(Config.PATH_PROJECT)
