# -*- coding: utf-8 -*-
import random
import time
import traceback

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait

from common.config import Config
from utils import g
from utils.log import Log

logger = Log()
class Helper(object):

    """打开浏览器"""
    driver = webdriver.Chrome(Config.PATH_PROJECT + "\libs\chromedriver.exe")

    def open(self):
        """
        打开默认站点
        """
        self.driver.delete_all_cookies()
        # self.driver.maximize_window()
        self.driver.get(Config.URL)

    def openUrl(self, url):
        """
        打开自定义站点
        :param url: url
        """
        self.driver.delete_all_cookies()
        # self.driver.maximize_window()
        self.driver.get(url)

    def close(self):
        """
        关闭driver
        """
        self.driver.quit()

    def inputContent(self, xpath, value):
        """
        在文本框中录入字段值
        :param xpath: xpath
        :param value: 文本内容
        """
        self.waitElementLoaded(xpath)
        self.driver.find_element_by_xpath(xpath).send_keys(value)

    def clearContent(self, xpath):
        """
        清除文本框中的内容
        :param xpath:
        """
        self.waitElementLoaded(xpath)
        self.driver.find_element_by_xpath(xpath).clear()

    def clickElement(self, xpath):
        """
        点击页面中的元素
        :param xpath:
        """
        self.waitElementLoaded(xpath)
        self.driver.find_element_by_xpath(xpath).click()

    def isElementSelected(self, xpath):
        """
        判断元素是否被勾选
        :param xpath:
        """
        self.waitElementLoaded(xpath)
        element = self.driver.find_element_by_xpath(xpath)
        isSelected = element.is_selected()
        print("isSelected=" + str(isSelected))
        return isSelected

    def waitElementLoaded(self, xpath):
        """
        隐式等待 默认等待3秒
        :param xpath: xpath
        """
        try:
            WebDriverWait(self.driver, 3).until(lambda driver: self.driver.find_element_by_xpath(xpath))
        except TimeoutException as e:
            print(xpath + "元素等待超时")
            raise Exception(xpath + "元素等待超时")

    def waitElementLoadedByTime(self, seconds, xpath):
        """
        显式等待
        :param seconds: 时间单位秒
        :param xpath: xpath
        """
        try:
            WebDriverWait(self.driver, seconds).until(lambda driver: self.driver.find_element_by_xpath(xpath))
        except TimeoutException as e:
            print(xpath + "元素等待超时")
            raise Exception(xpath + "元素等待超时")

    def sleep(self, seconds):
        """
        等待固定时间
        :param seconds:
        """
        time.sleep(seconds)

    def get_screenshot_path(self):
        """
        说明：
            截图并会在测试报告中体现
            单独调试TestCase时，可以在“\report\tempimg\”目录下查看截图
        """
        s = traceback.extract_stack()
        random_int = random.randint(10000000, 99999999)  # 随机数
        # 图片名称 需要拼上测试案例相关信息
        screenshot_name = (self.__module__
                           + "."
                           + self.__class__.__name__
                           + "."
                           + s[-2][2]
                           + "_"
                           + str(random_int)
                           + ".png")
        path = Config.PATH_REPORT_SCREENSHOT + "\\" + screenshot_name
        # 截图保存
        logger.info("截图保存路径为：" + path)
        self.driver.get_screenshot_as_file(path)
        g.SCREENSHOTS_NAME.append(screenshot_name)
