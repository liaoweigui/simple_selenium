# -*- coding: utf-8 -*-

import unittest

from utils.log import Log

logger = Log()


class NormalTest(unittest.TestCase):
    def setUp(self):
        logger.info("前置测试条件")

    def tearDown(self):
        logger.info("结束测试条件")

    def testadd(self):
        raise Exception("制造一个错误")
        logger.info('1+1=%s' % (1 + 1))

    def testsub2(self):
        logger.info('3-2=%s' % (3 - 2))

    def testsub1(self):
        logger.info('3-3=%s' % (3 - 3))

    def mul(self):
        logger.info('3*1=%s' % (3 * 1))
