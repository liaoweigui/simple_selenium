# -*- coding: utf-8 -*-
import unittest

from action.chsiregister import ChsiRegister
from common.helper import Helper
from utils.CommonUtils import CommonUtils
from utils.log import Log

logger = Log()


class ChsiRegistereTest(unittest.TestCase):
    helper = Helper()

    @classmethod  # 执行整个TestCase中只调用一次setUp
    def setUpClass(cls):  # 案例初始化时执行 必填
        cls.chsregister = ChsiRegister()

    @classmethod  # 执行整个TestCase中只调用一次tearDown
    def tearDownClass(cls):  # 案例结束时执行 必填
        cls.helper.close()

    def test_chsi_register(self):
        self.helper.openUrl("https://account.chsi.com.cn/account/preregister.action")
        self.chsregister.register(CommonUtils.RandomPhone(), CommonUtils.RandomNumber(), CommonUtils.RandomPassword(10),
                                  CommonUtils.RandomName(), CommonUtils.RandomIdCard(), CommonUtils.RandomEmail())
        self.helper.get_screenshot_path()
