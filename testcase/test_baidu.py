# -*- coding: utf-8 -*-
import unittest

from common.helper import Helper
from utils.log import Log

logger = Log()


class BaiDuTest(unittest.TestCase):
    helper = Helper()

    def test_baidu_ok(self):
        logger.info("测试test_baidu_ok开始")
        self.helper.openUrl("https://www.baidu.com/")
        xpath_input = '//input[@id="kw"]'
        xpath_submit = '//input[@id="su"]'
        self.helper.inputContent(xpath_input, "python")
        self.helper.clickElement(xpath_submit)
        self.helper.sleep(2)
        # 断言
        self.assertEqual(self.helper.driver.title, "python_百度搜索")
        logger.info("测试test_baidu_ok结束")

    def test_baidu_element_not_find(self):
        logger.info("测试test_baidu_element_not_find开始")
        self.helper.openUrl("https://www.baidu.com/")
        xpath_input = '//input[@id="kw123"]'
        xpath_submit = '//input[@id="su"]'
        self.helper.inputContent(xpath_input, "python")
        self.helper.clickElement(xpath_submit)
        self.helper.sleep(2)
        # 断言
        self.assertEqual(self.helper.driver.title, "Selenium_百度搜索_断言")
        logger.info("测试test_baidu_element_not_find结束")
