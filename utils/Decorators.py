# -*- coding: utf-8 -*-



# def exception_hadle_screenshot(function_to_decorate):
#     """
#     发生异常时，捕获异常界面截图和记录异常信息（用于WEB自动化方法上）
#     :param function_to_decorate:
#     """
#     helper = Helper()
#
#     def a_wrapper_accepting_arguments(*arg, **kwargs):
#         try:
#             function_to_decorate(*arg, **kwargs)
#         except Exception as e:
#             # 捕获异常界面截图
#             helper.driver.get_screenshot_as_file(Config.FILE_PATH_ERROR_SCREENSHOT)
#             print(str(e))
#             ex = Exception(str(e))
#             raise ex
#
#     return a_wrapper_accepting_arguments
#
# def exception_hadle(function_to_decorate):
#     """
#     发生异常时，记录异常信息（用于非WEB自动化方法上）
#     :param function_to_decorate:
#     """
#     def a_wrapper_accepting_arguments(*arg, **kwargs):
#         try:
#             function_to_decorate(*arg, **kwargs)
#         except Exception as e:
#             print(str(e))
#             ex = Exception(str(e))
#             raise ex
#
#     return a_wrapper_accepting_arguments


class Singleton(object):
    """
        单例模式（装饰类的类装饰器）
    """

    instances = {}

    def __init__(self, cls: type):
        self.cls = cls
        self.cls_name = cls.__name__

    def __call__(self, *args, **kwargs):
        if self.cls_name not in __class__.instances:
            __class__.instances[self.cls_name] = self.cls(*args, **kwargs)
        return __class__.instances[self.cls_name]