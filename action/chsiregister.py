# -*- coding: utf-8 -*-

# 学网信注册
from common.helper import Helper


class ChsiRegister(object):
    helper = Helper()

    def register(self, phone, code, password, name, idno, email):
        """
        学信网注册
        :param phone:
        :param code:
        :param password:
        :param name:
        :param idno:
        :param email:
        """
        xpath_phone = "//input[@id='mphone']"
        xpath_code = "//input[@id='vcode']"
        xpath_pwd = "//input[@id='password']"
        xpath_pwd1 = "//input[@id='password1']"
        xpath_xm = "//input[@id='xm']"
        xpath_sfzh = "//input[@id='sfzh']"
        xpath_email = "//input[@id='email']"
        xpath_btn_reg = "//input[@id='newbutton']"
        # 手机号
        self.helper.inputContent(xpath_phone, phone)
        # 短信验证码
        self.helper.inputContent(xpath_code, code)
        # 密码
        self.helper.inputContent(xpath_pwd, password)
        # 密码确认
        self.helper.inputContent(xpath_pwd1, password)
        # 姓名
        self.helper.inputContent(xpath_xm, name)
        # 证件号码
        self.helper.inputContent(xpath_sfzh, idno)
        # 邮箱
        self.helper.inputContent(xpath_email, email)
        # 点击“立即注册”按钮
        self.helper.clickElement(xpath_btn_reg)
