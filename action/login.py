# -*- coding: utf-8 -*-

# 登录、退出
from common.helper import Helper


class Login(object):
    helper = Helper()

    def login(self, username, password):
        xpath_username = "//input[@id='account']"
        xpath_password = "//input[@id='password']"
        xpath_btn_login = "//span[@class='ant-form-item-children']/button[@type='button']"
        # 输入用户名
        self.helper.inputContent(xpath_username, username)
        # 输入密码
        self.helper.inputContent(xpath_password, password)
        # 点击“登录”按钮
        self.helper.clickElement(xpath_btn_login)


class Logout(object):
    helper = Helper()

    def logout(self):
        self.helper.driver.delete_all_cookies()
        self.helper.driver.refresh()
